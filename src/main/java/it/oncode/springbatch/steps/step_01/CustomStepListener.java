package it.oncode.springbatch.steps.step_01;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

public class CustomStepListener implements StepExecutionListener {

    @Override
    public void beforeStep(StepExecution stepExecution) {
        System.out.println("--- listening before step ...");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        System.out.println("--- listening after step with ExitStatus: " + stepExecution.getExitStatus().getExitCode());
        return stepExecution.getExitStatus();
    }
}
