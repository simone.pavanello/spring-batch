package it.oncode.springbatch.job;

import it.oncode.springbatch.steps.step_01.CustomStepListener;
import it.oncode.springbatch.steps.step_01.CustomStepTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job customJob() {
        return jobBuilderFactory
                .get("customJob")
                .listener(customJobListener())
                .start(customStepOne())
                .build();
    }

    @Bean
    public Step customStepOne() {
        return stepBuilderFactory
                .get("customStepOne")
                .listener(customStepListener())
                .tasklet(customStepTasklet())
                .build();
    }

    @Bean
    public CustomStepTasklet customStepTasklet() {
        return new CustomStepTasklet();
    }

    @Bean
    public CustomStepListener customStepListener() {
        return new CustomStepListener();
    }

    @Bean
    public CustomJobListener customJobListener() {
        return new CustomJobListener();
    }

}
