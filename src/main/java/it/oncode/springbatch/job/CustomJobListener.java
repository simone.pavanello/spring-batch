package it.oncode.springbatch.job;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

public class CustomJobListener implements JobExecutionListener {


    @Override
    public void beforeJob(JobExecution jobExecution) {
        System.out.println("--- listening before job ...");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println("--- listening after job with ExitStatus: " + jobExecution.getExitStatus().getExitCode());
    }
}
